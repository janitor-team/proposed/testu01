/* testu01/examples/fbirth.c variant which prints tables in LaTeX style */

#include <testu01/fcong.h>
#include <testu01/ffam.h>
#include <testu01/fcho.h>
#include <testu01/fmarsa.h>

int main (void)
{
   ffam_Fam *fam;
   fcho_Cho *chon;
   fcho_Cho *chod;
   fcho_Cho2 *cho;

   ftab_Style = ftab_Latex; /* default value: ftab_Plain */

   fam = fcong_CreateLCGPow2 (NULL, 10, 30, 1);
   chon = fcho_CreateSampleSize (1.0/3.0, 1, 0, NULL, "n");
   chod = fmarsa_CreateBirthEC (1, 2, 1.0);
   cho = fcho_CreateCho2 (chon, chod);
   fmarsa_BirthdayS1 (fam, NULL, cho, 1, 0, 2, 1, 21, 1, 5, 1);
   fcho_DeleteCho2 (cho);
   fmarsa_DeleteBirthEC (chod);
   fcho_DeleteSampleSize (chon);
   fcong_DeleteLCGPow2 (fam);
   return 0;
}
