#include <testu01/unif01.h>
#include <testu01/ufile.h>
#include <testu01/scatter.h>

int main (void)
{
   unif01_Gen *gen;
   gen = ufile_CreateReadText ("RandomOrg.pts", 100000);
   scatter_PlotUnif (gen, "RandomOrg");
   ufile_DeleteReadText (gen);
   return 0;
}
